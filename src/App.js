import React from 'react';
import './App.css';
import Inputs from './components/inputs';
import TableView from './components/tableView';

export default class App extends React.Component {
  constructor(props){
    super(props);
    this.state = { table : [] };
  }

  dataUpdate = (data) => {
    this.setState({table : data});
    console.log(this.state.table);
  }

  render () {
    return (
      <div>
        <Inputs dataHandler={ this.dataUpdate } table={ this.state.table } />
        <TableView dataHandler={ this.dataUpdate } table={ this.state.table } />
      </div>
    );
  }
}
