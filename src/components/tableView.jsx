import React from "react";

export default class TableView extends React.Component {
    constructor(props){
        super(props);
        //this.state = { table : this.props.table }
    }

    deleteFunction = (x) => {
        let index = --x;
        this.props.table.splice(index, 1);
        //this.setState({table : tab});
        this.props.dataHandler([...this.props.table]);
    }

    render () {
        let i = 1;
        return(
            <table style={{ "border": "solid brown 2px" }}>
                <thead>
                    <tr>
                        <th>Lp.</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Mood</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        this.props.table.length !=0 ? this.props.table.map((x, i) => {
                            return (
                                <tr>
                                    <th>{++i}</th>
                                {
                                    x.map((zz) => {
                                       return <th>{zz}</th>
                                    })
                                }
                                    <th><button onClick={ (x) => {this.deleteFunction(i)}}>Delete</button></th>
                                </tr>
                            )
                        }) : <h1 style={{columnSpan: "5", display: "table-cell"}}>Nie podano danych</h1> 
                        //ten columnSpan niedziała ale ogólnie jest giga dobrze
                    }
                </tbody>
            </table>
        );
    }
}