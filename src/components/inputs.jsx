import React, { createRef } from "react";

export default class Inputs extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.input1 = React.createRef();
        this.input2 = React.createRef();
        this.input3 = React.createRef();
      }

      dataFromInputs = () => {
        let tab = this.props.table;
        tab.push([this.input1.current.value, this.input2.current.value, this.input3.current.value]);
        this.props.dataHandler([...tab]);
      }
    
      render(){
        return (
        <div>
            <input type='text' placeholder="Name" ref={this.input1} />
            <input type='text' placeholder="Surname" ref={this.input2} />
            <input type='text' placeholder="Mood" ref={this.input3} />
            <button onClick={() => this.dataFromInputs()}> Add </button>
        </div>
        );
      }
}